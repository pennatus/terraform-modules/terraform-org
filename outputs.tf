output "org_project_id" {
  value = length(var.ORG_PROJECT_ID) == 0 ? google_project.org_project[0].project_id : var.ORG_PROJECT_ID
  description = "This is the project id hosting the org resources"
}

output "org_project_number" {
  value = data.google_project.org_project.number
  description = "This is the project id hosting the org resources"
}

output "folder_ids" {
  value = module.folders.ids
  description = "Map of environment name to folder id"
}

output "vault_admin_policies" {
  value = module.org_policies.vault_admin_policies
  description = "Vault policies created with super admin access to everything in Vault.  Give out with care"
}

output "vault_token_creator_policies" {
  value = module.org_policies.vault_token_creator_policies
  description = "Vault policies needed to create child tokens - necessary if using a token as a Vault provider in Terraform"
}