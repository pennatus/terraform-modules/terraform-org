data "vault_policy_document" "policy_doc_product_viewer" {
  count = var.ENABLE_PRODUCT ? length(var.ENVIRONMENTS) : 0
  rule {
    path         = "/product/${var.ENVIRONMENTS[count.index]}/*"
    capabilities = ["read"]
  }
}

data "vault_policy_document" "policy_doc_product_editor" {
  count = var.ENABLE_PRODUCT ? length(var.ENVIRONMENTS) : 0
  rule {
    path         = "/product/${var.ENVIRONMENTS[count.index]}/*"
    capabilities = ["read", "update", "list"]
  }
}

data "vault_policy_document" "policy_doc_product_owner" {
  count = var.ENABLE_PRODUCT ? length(var.ENVIRONMENTS) : 0
  rule {
    path         = "/product/${var.ENVIRONMENTS[count.index]}/*"
    capabilities = ["create", "read", "update", "delete", "list"]
  }
  rule {
    path         = "/org/gcp/token/${substr(var.ENVIRONMENTS[count.index], 0, 3)}-*"
    capabilities = ["read"]
  }
}

resource "vault_policy" "product_viewer" {
  count = var.ENABLE_PRODUCT ? length(var.ENVIRONMENTS) : 0
  name   = "product-${var.ENVIRONMENTS[count.index]}-viewer"
  policy = data.vault_policy_document.policy_doc_product_viewer[count.index].hcl
}

resource "vault_policy" "product_editor" {
  count = var.ENABLE_PRODUCT ? length(var.ENVIRONMENTS) : 0
  name   = "product-${var.ENVIRONMENTS[count.index]}-editor"
  policy = data.vault_policy_document.policy_doc_product_editor[count.index].hcl
}

resource "vault_policy" "product_owner" {
  count = var.ENABLE_PRODUCT ? length(var.ENVIRONMENTS) : 0
  name   = "product-${var.ENVIRONMENTS[count.index]}-owner"
  policy = data.vault_policy_document.policy_doc_product_owner[count.index].hcl
}
