data "vault_policy_document" "policy_doc_org_owner" {
  count =  var.ENABLE_ORG ? 1 : 0
  rule {
    path         = "/org/*"
    capabilities = ["create", "read", "update", "delete", "list"]
  }
}

resource "vault_policy" "org_owner" {
  count = var.ENABLE_ORG ? 1 : 0
  name = "org-owner"
  policy = data.vault_policy_document.policy_doc_org_owner[count.index].hcl
}
