output "vault_admin_policies" {
  value = vault_policy.vault_admin
  description = "Vault policies created with super admin access to everything in Vault.  Give out with care"
}

output "vault_token_creator_policies" {
  value = vault_policy.token_creator
  description = "Vault policies needed to create child tokens - necessary if using a token as a Vault provider in Terraform"
}