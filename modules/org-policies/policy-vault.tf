data "vault_policy_document" "policy_doc_vault_owner" {
  rule {
    path = "auth/*"
    capabilities = ["create", "read", "update", "delete", "list"]
  }
  rule {
    path = "sys/auth"
    capabilities = ["read"]
  }
  rule {
    # List, create, update, and delete auth backends
    path = "sys/auth/*"
    capabilities = ["create", "read", "update", "delete"]
  }
  rule {
    # List existing policies
    path = "sys/policy"
    capabilities = ["read"]
  }
  rule {
    # Create and manage policies
    path = "sys/policy/*"
    capabilities = ["create", "read", "update", "delete", "list"]
  }
  rule {
    # Create and manage ACL policies
    path = "sys/policies/acl"
    capabilities = ["read"]
  }
  rule {
    # Create and manage ACL policies
    path = "sys/policies/acl/*"
    capabilities = ["create", "read", "update", "delete", "list"]
  }
  rule {
    # List, create, update, and delete org secrets
    path = "org/*"
    capabilities = ["create", "read", "update", "delete", "list"]
  }
  rule {
    # List, create, update, and delete product secrets
    path = "product/*"
    capabilities = ["create", "read", "update", "delete", "list"]
  }
  rule {
    # Manage secret backends
    path = "sys/mounts"
    capabilities = ["read"]
  }
  rule {
    path = "sys/mounts/*"
    capabilities = ["create", "read", "update", "delete", "list"]
  }
}

resource "vault_policy" "vault_owner" {
  name = "vault-owner"
  policy = data.vault_policy_document.policy_doc_vault_owner.hcl
}

# Manage auth backends broadly across Vault
# Allow admin the ability to manage identites - i.e. groups
data "vault_policy_document" "policy_doc_vault_admin" {
  rule {
    path = "identity/*"
    capabilities = ["create", "read", "update", "delete", "list", "sudo"]
  }
  rule {
    path = "auth/*"
    capabilities = ["create", "read", "update", "delete", "list", "sudo"]
  }
  rule {
    path = "sys/auth"
    capabilities = ["create", "read", "update", "delete", "list"]
  }
  rule {
    # List, create, update, and delete auth backends
    path = "sys/auth/*"
    capabilities = ["create", "read", "update", "delete", "sudo"]
  }
  rule {
    # Used to move an existing secret engine mount point
    path = "sys/remount"
    capabilities = ["update", "sudo"]
  }
  rule {
    # List existing policies
    path = "sys/policy"
    capabilities = ["read"]
  }
  rule {
    # Create and manage policies
    path = "sys/policy/*"
    capabilities = ["create", "read", "update", "delete", "list"]
  }
  rule {
    # View ACL policies
    path = "sys/policies/acl"
    capabilities = ["list", "read"]
  }
  rule {
    # Create and manage ACL policies
    path = "sys/policies/acl/*"
    capabilities = ["create", "read", "update", "delete", "list", "sudo"]
  }
  rule {
    # List, create, update, and delete org secrets
    path = "org/*"
    capabilities = ["create", "read", "update", "delete", "list", "sudo"]
  }
  rule {
    # List, create, update, and delete product secrets
    path = "product/*"
    capabilities = ["create", "read", "update", "delete", "list", "sudo"]
  }
  rule {
    # Manage secret backends
    path = "sys/mounts"
    capabilities = ["read"]
  }
  rule {
    # Manage and manage secret backends broadly across Vault.
    path = "sys/mounts/*"
    capabilities = ["create", "read", "update", "delete", "list", "sudo"]
  }
  rule {
    # Read health checks
    path = "sys/health"
    capabilities = ["read", "sudo"]
  }
}

resource "vault_policy" "vault_admin" {
  name = "vault-admin"
  policy = data.vault_policy_document.policy_doc_vault_admin.hcl
}