data "vault_policy_document" "policy_doc_token_creator" {
  rule {
    path         = "/auth/token/create"
    capabilities = ["update"]
  }
}

resource "vault_policy" "token_creator" {
  name = "token-creator"
  policy = data.vault_policy_document.policy_doc_token_creator.hcl
}