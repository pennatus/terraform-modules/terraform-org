data "vault_policy_document" "policy_doc_oidc_default_viewer" {
  count = var.ENABLE_OIDC_DEFAULT ? 1 : 0
  rule {
    # FIXME: need to read this from remote state
    path         = "/org/gcp/token/prd-shared-vi"
    capabilities = ["read"]
  }
}

resource "vault_policy" "oidc_default_viewer" {
  count = var.ENABLE_OIDC_DEFAULT ? 1 : 0
  name = var.OIDC_DEFAULT_POLICY
  policy = data.vault_policy_document.policy_doc_oidc_default_viewer[count.index].hcl
}