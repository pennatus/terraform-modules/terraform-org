variable "ENVIRONMENTS" {
  description = "List of possible environments, use the global value."
  type = list
}

variable "OIDC_DEFAULT_POLICY" {
  description = "Name of the default OIDC policy (ex: team-viewer)"
  type = string
}

variable "ENABLE_PRODUCT" {
  description = "Define classes for environment for each product scope.  A product scope refers to each instance of product created by the Product Factory Module. Classes: editor, owner, viewer"
  type = bool
  default = true
}

variable "ENABLE_ORG" {
  description = "The org policy grants classes that can modify any /org/* resources.  This should only be granted to Vault tokens which manage the entire organisation which is managed by the Terraform workspace is-org. Classes: owner"
  type = bool
  default = true
}


variable "ENABLE_OIDC_DEFAULT" {
  description = "The team policy grants a viewer class that allows GCP users to auth and use glob This should only be granted to Vault tokens which manage the entire organisation which is managed by the Terraform workspace is-org. Classes: viewer"
  type = bool
  default = false
}