# org-policies

Manage organisation policies, based from Pennatus module and adapted for NuSutus.

Good Practices:

- Audit the default module to make sure the policies are scoped correctly
- If using the Pennatus module, scope to only audited versions
- Consider cloning the Pennatus module and extended and patching as needed

This module requires the Vault provider to be configured before any policies will be appiled.

## Policy Classes

A policy class is a conceptual definition of the base permissions granted to a specific scope.  For example a product-owner has full permissions for a specific product (ex: Shared).

### admin

An admin is a special class reserved for Vault administrators and the is-org workspace.

### owner

An owner has full permissions to create, manage and destroy.  In some cases Sudo may be granted if necessary.  This class should be reserved for admin activities like Terraform Workspace Service Accounts or Human Admins.

Permissions: Create, Read, Update, Delete, List, Sudo

### editor

An editor can modify existing resources, but not create or delete resources. This can be used for Configuration Service Accounts like an Ansible.

Permissions: Read, Update, List

### viewer

A viewer is a read-only class used by observers, like monitoring.

Permission: Read

### creator

A Creator can create resources, but nothing else.  The main use case is to generate Vault tokens for Terraform providers (which create a child tokens from their token).

Permission: Create

## Usage

```hcl

module "org_policies" {
  source = "git::https://gitlab.com/pennatus/infrastructure//modules/org-policies"
  ENVIRONMENTS = var.ENVIRONMENTS
  ENABLE_DB_INSTANCE = true
  ENABLE_ORG = true
  ENABLE_PRODUCT = true
  ENABLE_OIDC_DEFAULT = true
}

```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| ENVIRONMENTS | List of possible environments, use the global value. | list | n/a | yes |
| OIDC\_DEFAULT\_POLICY | Name of the default OIDC policy \(ex: team-viewer\) | string | n/a | yes |
| ENABLE\_DB\_INSTANCE | The db-instance-engine policy allows the token holder to create new db secret engine mounts and rotate the root secret on each DB instance.  Product owners can manage specific database credentials. Classes: owner | bool | `"false"` | no |
| ENABLE\_OIDC\_DEFAULT | The team policy grants a viewer class that allows GCP users to auth and use glob This should only be granted to Vault tokens which manage the entire organisation which is managed by the Terraform workspace is-org. Classes: viewer | bool | `"false"` | no |
| ENABLE\_ORG | The org policy grants classes that can modify any /org/\* resources.  This should only be granted to Vault tokens which manage the entire organisation which is managed by the Terraform workspace is-org. Classes: owner | bool | `"true"` | no |
| ENABLE\_PRODUCT | Define classes for environment for each product scope.  A product scope refers to each instance of product created by the Product Factory Module. Classes: editor, owner, viewer | bool | `"true"` | no |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
