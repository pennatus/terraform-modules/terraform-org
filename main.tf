data "google_organization" "org" {
  domain = var.DOMAIN
}

data "google_billing_account" "acct" {
  display_name = var.BILLING_NAME_GENERAL
}

data "google_project" "org_project" {
  project_id = length(var.ORG_PROJECT_ID) == 0 ? google_project.org_project[0].id : var.ORG_PROJECT_ID
}

resource "random_string" "org_project_id_suffix" {
  length = 23
  upper = false
  special = false
}

# Create the org project if an ORG_PROEJCT_ID has not been specified
resource "google_project" "org_project" {
  count = length(var.ORG_PROJECT_ID) == 0 ? 1 : 0
  name = var.ORG_PROJECT_NAME
  project_id = "is-org-${random_string.org_project_id_suffix.result}"
  org_id  = data.google_organization.org.org_id
  billing_account = data.google_billing_account.acct.id

  # Wait a little extra time because it seems like the billing takes awhile
  # to actually be setup
  provisioner "local-exec" {
    command = "sleep 30"
  }
}

# Create the top level folders for each environment type
module "folders" {
  source  = "terraform-google-modules/folders/google"  

  parent  = "${data.google_organization.org.name}"

  names = var.ENVIRONMENTS
}

# Setup the recommended initial org policies
module "org_policies" {
  source = "./modules/org-policies"
  ENABLE_ORG = var.ENABLE_ORG
  ENABLE_PRODUCT = var.ENABLE_PRODUCT
  ENABLE_OIDC_DEFAULT = var.ENABLE_OIDC_DEFAULT
  ENVIRONMENTS = var.ENVIRONMENTS
  OIDC_DEFAULT_POLICY = var.OIDC_DEFAULT_POLICY
}
