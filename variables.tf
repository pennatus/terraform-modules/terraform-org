# ---------------------------------------------------------------------------------------------------------------------
# REQUIRED PARAMETERS
# These parameters must be supplied and are secretive
# ---------------------------------------------------------------------------------------------------------------------

variable "ORG_PROJECT_ID" {
  description = "Project ID that will be hosting the main Org instance.  Leave empty to let this module create a project for you."
  type        = string
  default     = ""
}

variable "ORG_PROJECT_NAME" {
  description = "Project name to assign project hosting the Org resources."
  type        = string
  default     = ""
}

variable "DOMAIN" {
  description = "This is the domain for which your GCP account is hosted under.  Supplied by CookieCutter template."
  type        = string
}

variable "BILLING_NAME_GENERAL" {
  description = "Display name for a billing account"
  type = string
  default = "My Billing Account"
}

# ---------------------------------------------------------------------------------------------------------------------
# OPTIONAL PARAMETERS
# These parameters can be specified to override defaults.
# ---------------------------------------------------------------------------------------------------------------------

variable "ENVIRONMENTS" {
  description = "List of possible environments for all projects"
  type = list
  default = ["dev", "stg", "prd"]
}

variable "OIDC_DEFAULT_POLICY" {
  description = "Name of the default OIDC policy (ex: team-viewer)"
  type = string
  default = "team-viewer"
}

variable "ENABLE_VAULT" {
  description = "Define classes for Vault, the admin class allows the token holder to manage all aspects of Vault without being full root.  The owner class is modelled after Terraforms example of a provisioner which is intended to be a hybrid of an owner and admin.  Classes: admin, owner"
  type = bool
  default = true
}

variable "ENABLE_TOKEN" {
  description = "Token creator is required if a vault provider is used and grants the permission to create a child token from the issued token.  For example, the token issued to a terraform.io workspace should be granted the token-create policy.  Classes: creator"
  type = bool
  default = true
}

variable "ENABLE_PRODUCT" {
  description = "Define classes for environment for each product scope.  A product scope refers to each instance of product created by the Product Factory Module. Classes: editor, owner, viewer"
  type = bool
  default = true
}

variable "ENABLE_ORG" {
  description = "The org policy grants classes that can modify any /org/* resources.  This should only be granted to Vault tokens which manage the entire organisation which is managed by the Terraform workspace is-org. Classes: owner"
  type = bool
  default = true
}


variable "ENABLE_OIDC_DEFAULT" {
  description = "The team policy grants a viewer class that allows GCP users to auth and use glob This should only be granted to Vault tokens which manage the entire organisation which is managed by the Terraform workspace is-org. Classes: viewer"
  type = bool
  default = true
}